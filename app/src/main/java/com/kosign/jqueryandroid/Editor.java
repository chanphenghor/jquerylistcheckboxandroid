package com.kosign.jqueryandroid;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
public class Editor extends WebView {


  private static final String SETUP_HTML = "file:///android_asset/test.html";

  public Editor(Context context) {
    this(context, null);
  }

  public Editor(Context context, AttributeSet attrs) {
    this(context, attrs, android.R.attr.webViewStyle);
  }

  @SuppressLint("SetJavaScriptEnabled")
  public Editor(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    setVerticalScrollBarEnabled(false);
    setHorizontalScrollBarEnabled(false);
    getSettings().setJavaScriptEnabled(true);
    setWebChromeClient(new WebChromeClient());
    loadUrl(SETUP_HTML);
  }
//  @Override
//  public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
//    return new BaseInputConnection(this, false); //this is needed for #dispatchKeyEvent() to be notified.
//  }
//
//  @Override
//  public boolean dispatchKeyEvent(KeyEvent event) {
//    boolean dispatchFirst = super.dispatchKeyEvent(event);
//    // Listening here for whatever key events you need
//    if (event.getAction() == KeyEvent.ACTION_UP)
//      switch (event.getKeyCode()) {
//        case KeyEvent.KEYCODE_ENTER:
//          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            evaluateJavascript("javascript:hoge()", null);
//          } else {
//            loadUrl("javascript:huge()");
//          }
//          break;
//      }
//    return dispatchFirst;
//  }

}
