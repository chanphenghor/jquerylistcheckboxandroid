	//$('#editor').bind("DOMSubtreeModified",function(){
		//if($("input[name=chk_b]").is(":checked")){
			
		//}
		//else{
			
		//}
	//});
	
$(function(){ 
	var myul = $("#myul");
	var chk_list = false;
	var divParseHtml = $("#div-parse-html");
	
	
   $('input[name="chkbox"]').each(function() {
        if ($(this).is(":checked")) {
			$(this).closest('.def_checkbox').find('.img-check').attr("src","chk_checked.png");
            
        }else{
			$(this).closest('.def_checkbox').find('.img-check').attr("src","chk_unchecked.png");
		}
    });
   $('#btn-parse-html').click(function() {
		$('.input-editable').each(function() {
			if ($(this).val() == "") {
				var index = $(this).parentsUntil("li").parent().index();
				$('#myul li').eq(index).remove();
			}
		});
        var listItems = $("#myul li");
		var strParseHtml = '<div class="check_list"><ul>';
		var i = 1;
		for (let li of listItems) {
			let product = $(li);
			var isChecked = product.find('input[name="chkbox"]').is(":checked");
			var strChecked = isChecked ? " checked='checked' " : "";
			strParseHtml += "<li>"+
							'<div class="def_checkbox"><input id="a'+i+'" type="checkbox"'+ strChecked +'/><label for="a'+i+'">'+ product.find(".input-editable").val() +'</label></div>'+
							"</li>"
							i++;
		}
		strParseHtml += "</ul></div>";
		$("#div-parse-html").text(strParseHtml);
    });
	
	//function addListItem(e,index) {
	//	if(e.type==="keydown" && e.which !== 13) return;
	//	e.preventDefault();
	//	console.log("li size "+$('#myul li').length + " index at "+index);
	//	alert(1);
	//	if($('#myul li').length - 1 == index){
	//		myul.append('<li>'+
	//				'<div class="def_checkbox">'+
	//					'<img src="chk_unchecked.png"  class="img-check" width="12" height="12" align="middle">'+
	//					'<input type="checkbox" class="hide-element" name="chkbox"> '+
	//					'<input type="text" class="input-editable" value="">'+
	//				'</div>'+
	//			'</li>');	
	//	}	
	//}
	function addListItem(index) {
		var strAppend = '<li>'+
					'<div class="def_checkbox">'+
						'<img src="chk_unchecked.png"  class="img-check" width="12" height="12" align="middle">'+
						'<input type="checkbox" class="hide-element" name="chkbox"> '+
						'<input type="text" class="input-editable" value="">'+
					'</div>'+
				'</li>';
		$('.input-editable').each(function() {
			if ($(this).val() == "") {
				var index = $(this).parentsUntil("li").parent().index();
				$('#myul li').eq(index).remove();
			}
		});
		if(typeof index === 'undefined' || index === null){
			myul.append(strAppend);	
		}else{
			if($('#myul li').length - 1 == index){
				myul.append(strAppend);	
				$('#myul').children().last().find('.input-editable').focus();
			}else{
				$('#myul li').eq(index).after(strAppend);
				console.log($('#myul li').eq(index).html());
				$('#myul li').eq(index + 1 ).find('.input-editable').focus();
			}
		}
		
	}
	
	
	$(document).on("keydown",".input-editable", function(e){
		var keycode = e.charCode || e.keyCode;
			if (keycode  == 13) { 
				if(chk_list){
					if($(this).val() !== ""){
						var index = $(this).parentsUntil("li").parent().index();
						addListItem(index);
					}else{
						return false;
					}
				}
			}
		
	});
	
	var isRemoveBox = false;
	
	$(document).on("keyup",".input-editable", function(e){
		if(e.keyCode == 8){
			if($(this).val() == ""){
				if(isRemoveBox){
					isRemoveBox = false;
					var index = $(this).parentsUntil("li").parent().index();
					var data = $('#myul li').eq(index-1).find(".input-editable").val();
					$('#myul li').eq(index-1).find(".input-editable").focus().val('').val(data);
					$(this).parentsUntil("li").parent().remove();
					
					if(index == 1){
						$('#myul li').eq(0).find(".input-editable").focus().val('').val(data);
					}
					if($('#myul li').length === 0 ){
						$('#chk_list').prop('checked', false);
					}
				}
			}
		}
	});
	$(document).on("keydown",".input-editable", function(e){
		if(e.keyCode == 8){
			if($(this).val() == ""){
				isRemoveBox = true;
			}
		}
	});
	
	
   $(document).on('input propertychange paste',".input-editable", function() {
		chk_list = true;
		$('#chk_list').prop('checked', true);
	});
	
	$(document).on("click",".img-check", function(e){
		if ($(this).closest('.def_checkbox').find('.hide-element').is(":checked")) {
			$(this).closest('.def_checkbox').find('.hide-element').removeAttr("checked");
			$(this).attr("src","chk_unchecked.png");
        }else{
			$(this).closest('.def_checkbox').find('.hide-element').attr("checked","checked");
			$(this).attr("src","chk_checked.png");
		}
	});
	 $('#chk_list').change(function(e) {
        if($(this).is(":checked")) {
            chk_list = true;
			if ($('#myul li').length == 0){
				addListItem(null);
				$('#myul').children().last().find('.input-editable').focus();
			}
        }else{
			if ($('#myul li').length == 1){
				if($('#myul li').eq(0).find(".input-editable").val() == ""){
					$('#myul li').eq(0).remove();
				}
			}
			
			$('.input-editable').each(function() {
				if ($(this).val() == "") {
					var index = $(this).parentsUntil("li").parent().index();
					$('#myul li').eq(index).remove();
				}
			});
			chk_list = false;
		}
       
    });
});
